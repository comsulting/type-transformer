import { Direction, Selectable, SelectFunction, SelectOptions, Visibility } from './selection';

interface PropNameFn {
    (inObj, direction: Direction): string;
}

export interface ExposeOptions extends SelectOptions {
    propName?: string | PropNameFn;
}

const AlwaysExpose: SelectFunction = () => true; // tslint:disable-line variable-name
const AlwaysExclude: SelectFunction = () => false; // tslint:disable-line variable-name

export class Schema {
    inherits = false;
    private _defaultVisibility: Visibility | undefined;
    private _fields: Map<string, Field> = new Map<string, Field>();
    private _postProcessors: ObjectTransformer[] = [];

    constructor (readonly registry, readonly type: Function) { }

    getField(fieldName: string): Field {
        let field = this._fields.get(fieldName);
        if (!field) {
            field = new Field(fieldName);
            this._fields.set(fieldName, field);
        }
        return field;
    }

    get defaultVisibility() {
        if (this._defaultVisibility) {
            return this._defaultVisibility;
        }
        if (this.inherits) {
            const parent = this.getAncestor();
            if (parent) {
                return parent.defaultVisibility;
            }
        }
    }

    /**
     * Get all fields defined in this schema and its ancestors as an iterator.
     */
    get fields() {
        if (!this.inherits) {
            return this._fields.values();
        }
        const allFields = new Map<string, Field>();
        for (const member of this.inheritanceChain()) {
            for (const field of member._fields.values()) {
                if (!allFields.has(field.name)) {
                    allFields.set(field.name, field);
                }
            }
        }
        return allFields.values();
    }

    excludeAll() {
        this._defaultVisibility = Visibility.EXCLUDE;
        return this;
    }

    exposeAll() {
        this._defaultVisibility = Visibility.EXPOSE;
        return this;
    }

    inherit() {
        this.inherits = true;
        return this;
    }

    postProcess(postProcessFn: ObjectTransformationFunction, options?: SelectOptions) {
        this._postProcessors.push(new ObjectTransformer(postProcessFn, options));
        return this;
    }

    getPostProcessors(obj: any, direction: Direction): ObjectTransformer[] {
        return this._postProcessors
            .filter(postProcessor => {
                const visibility = postProcessor.selected(obj, direction);
                // if there is no explicit visibility we do the transform
                return !visibility || visibility === Visibility.EXPOSE;
            });
    }

    /**
     * Get a list of all schemas in the inheritance chain of this schema (including itself).
     */
    private * inheritanceChain() {
        let parentSchema: Schema | undefined = this;
        do {
            yield parentSchema;
        } while (parentSchema = parentSchema.getAncestor());
    }

    /**
     * Get the nearest ancestor schema.
     */
    private getAncestor() {
        let parentType = this.type;
        let parentSchema: Schema | undefined;
        while (Object.prototype !== (parentType = Object.getPrototypeOf(parentType))) {
            parentSchema = this.registry.getSchema(parentType);
            if (parentSchema) {
                return parentSchema;
            }
        }
    }
}

export class Field extends Selectable {
    nestedType;
    private _propName?: string | PropNameFn;
    private _transformers: ValueTransformer[] = [];

    constructor(readonly name: string) {
        super();
    }

    select(selectFn: SelectFunction) {
        this.addSelector(selectFn);
        return this;
    }

    exclude(options?: SelectOptions) {
        this.addSelector(AlwaysExclude, options);
        return this;
    }

    expose(options?: ExposeOptions) {
        this.addSelector(AlwaysExpose, options);
        if (options) {
            this._propName = options.propName;
        }
        return this;
    }

    nested(type) {
        this.nestedType = type;
        return this;
    }

    transform(transformFn: ValueTransformationFunction, options?: SelectOptions) {
        const transformer = new ValueTransformer(transformFn, options);
        this._transformers.push(transformer);
        return this;
    }

    getValueTransformers(obj: any, direction: Direction): ValueTransformer[] {
        return this._transformers
            .filter(transformer => {
                const visibility = transformer.selected(obj, direction);
                // if there is no explicit visibility we do the transform
                return !visibility || visibility === Visibility.EXPOSE;
            });
    }

    get propName(): string | PropNameFn {
        return this._propName || this.name;
    }
}

class Transformer<TransformFn extends Function> extends Selectable {
    constructor (readonly transformFn: TransformFn, options?: SelectOptions) {
        super();
        if (options) {
            this.addSelector(AlwaysExpose, options);
            // if we have options, we don't want the transformation if the
            // conditions in the options are not met
            this.defaultVisibility = Visibility.EXCLUDE;
        }
    }

    transform(inValue: any, targetObj: any, direction: Direction) {
        return this.transformFn(inValue, targetObj, direction);
    }
}

export interface ValueTransformationFunction {
    (value: any, obj: any, direction: Direction): any;
}

export class ValueTransformer extends Transformer<ValueTransformationFunction> { }

export interface ObjectTransformationFunction {
    (inObj: any, outObj: any, direction: Direction): any;
}

export class ObjectTransformer extends Transformer<ObjectTransformationFunction> { }
