import { defaultSchemaRegistry } from '../../src/registry';
import { exclude, expose, inherit, typedToPlain } from '../../src';

describe('inheritance', () => {
    afterEach(() => {
        defaultSchemaRegistry.clear();
    });

    it('uses a schema defined for a parent class', () => {
        class Person {
            name: string;
            @exclude()
            age: number;
        }
        @inherit()
        class Citizen extends Person {
            insuranceNo: number;
        }

        let citizen = new Citizen();
        citizen.name = 'Famous Person';
        citizen.age = 34;
        citizen.insuranceNo = 777;

        const plainCitizen = typedToPlain(citizen);
        expect(plainCitizen.age).toBeUndefined();
        expect(plainCitizen.insuranceNo).toBe(777);
    });

    it('bridges a gap in the inheritance chain', () => {
        class Person {
            name: string;
            @exclude()
            age: number;
        }
        class Citizen extends Person {
            insuranceNo: number;
        }
        @inherit()
        class King extends Citizen {
            might: number;
        }

        let king = new King();
        king.name = 'Famous Person';
        king.age = 34;
        king.insuranceNo = 777;
        king.might = 42;

        const plainKing = typedToPlain(king);
        expect(plainKing.age).toBeUndefined();
        expect(plainKing.insuranceNo).toBe(777);
        expect(plainKing.might).toBe(42);
    });

    it('overrides a setting in a child class', () => {
        class Person {
            name: string;
            @exclude()
            age: number;
        }
        @inherit()
        class Citizen extends Person {
            insuranceNo: number;
            @expose()
            age: number;
        }

        let citizen = new Citizen();
        citizen.name = 'Famous Person';
        citizen.age = 34;
        citizen.insuranceNo = 777;

        const plainCitizen = typedToPlain(citizen);
        expect(plainCitizen.age).toBeDefined();
    });

    it('inherits the default visibility', () => {
        @exclude()
        class Person {
            @expose()
            name: string;
            age: number;
        }
        @inherit()
        class Citizen extends Person {
            insuranceNo: number;
        }

        let citizen = new Citizen();
        citizen.name = 'Famous Person';
        citizen.age = 34;
        citizen.insuranceNo = 777;

        const plainCitizen = typedToPlain(citizen);
        expect(plainCitizen.age).toBeUndefined();
        expect(plainCitizen.insuranceNo).toBeUndefined();
    });
});
