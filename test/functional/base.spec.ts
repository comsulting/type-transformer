import { typedToPlain, plainToTyped, createRegistry, createSchema } from '../../src';
import { defaultSchemaRegistry } from '../../src/registry';


describe('basic functionality', () => {
    afterEach(() => {
        defaultSchemaRegistry.clear();
    });

    it('copies all properties from a typed object to a pojo', () => {
        class User {
            id: number;
            firstName: string;
            lastName: string;
            password: string;
        }

        const user = new User();
        user.firstName = 'Tim';
        user.lastName = 'Mundt';
        user.password = 'imsuperman';

        const plainUser = typedToPlain(user);
        expect(plainUser).not.toEqual(jasmine.any(User));
        expect(plainUser).toEqual({
            firstName: 'Tim',
            lastName: 'Mundt',
            password: 'imsuperman'
        });
    });

    it('copies all properties form a pojo to a typed object', () => {
        class User {
            firstName: string;
            lastName: string;
            password: string;
        }

        const plainUser = {
            firstName: 'Tim',
            lastName: 'Mundt',
            password: 'imsuperman'
        };

        const user = plainToTyped(plainUser, User);
        expect(user).toEqual(jasmine.any(User));
        expect({...user}).toEqual({
            firstName: 'Tim',
            lastName: 'Mundt',
            password: 'imsuperman'
        });
    });

    it('only copies non-excluded properties from a typed object to a pojo', () => {
        class User {
            id: number;
            firstName: string;
            lastName: string;
            password: string;
        }

        const schema = createSchema(User);
        schema.getField('password').exclude();

        const user = new User();
        user.firstName = 'Tim';
        user.lastName = 'Mundt';
        user.password = 'imsuperman';

        const plainUser = typedToPlain(user);
        expect(plainUser).not.toEqual(jasmine.any(User));
        expect(plainUser).toEqual({
            firstName: 'Tim',
            lastName: 'Mundt'
        });
    });

    it('only copies specifically included properties from an excluded typed object to a pojo',
    () => {
        class User {
            id: number;
            firstName: string;
            lastName: string;
            password: string;
        }

        const schema = createSchema(User);
        schema.excludeAll();
        schema.getField('lastName').expose();

        const user = new User();
        user.firstName = 'Tim';
        user.lastName = 'Mundt';
        user.password = 'imsuperman';

        const plainUser = typedToPlain(user);
        expect(plainUser).not.toEqual(jasmine.any(User));
        expect(plainUser).toEqual({
            lastName: 'Mundt'
        });
    });

    it('creates empty pojo from a typed object where fields are excluded by default', () => {
        class User {
            id: number;
            firstName: string;
            lastName: string;
            password: string;
        }

        const schema = createSchema(User);
        schema.excludeAll();

        const user = new User();
        user.firstName = 'Tim';
        user.lastName = 'Mundt';
        user.password = 'imsuperman';

        const plainUser = typedToPlain(user);
        expect(plainUser).not.toEqual(jasmine.any(User));
        expect(plainUser).toEqual({});
    });

    it('copies all properties form a pojo and transforms them to typed objects', () => {
        class Author {
            firstName: string;
            lastName: string;
        }

        class Book {
            title: string;
            author: Author;
        }

        const plainBook = {
            title: 'The Mythical man week',
            author: {
                firstName: 'Tim',
                lastName: 'Mundt'
            }
        };
        const schema = createSchema(Book);
        schema.getField('author').nested(Author);

        const book = plainToTyped(plainBook, Book);
        expect(book).toEqual(jasmine.any(Book));
        expect(book.author).toEqual(jasmine.any(Author));
    });

    it('uses a configured propName to put a value on a POJO', () => {
        class Book {
            title: string;
        }
        const schema = createSchema(Book);
        schema.getField('title').expose({propName: 'name'});

        const book = new Book();
        book.title = 'The Mythical man week';

        const plainBook = typedToPlain(book);
        expect(plainBook.name).toEqual(book.title);
    });

    it('uses a configured propName to get a value from a POJO', () => {
        class Book {
            title: string;
        }
        const schema = createSchema(Book);
        schema.getField('title').expose({propName: 'name'});

        const plainBook = {
            name: 'The Mythical man week'
        };
        const book = plainToTyped(plainBook, Book);
        expect(book.title).toEqual(plainBook.name);
    });

    it('uses a configured propName function to put a value on a POJO', () => {
        class Book {
            title: string;
        }
        const schema = createSchema(Book);
        schema.getField('title').expose({propName: () => 'name'});

        const book = new Book();
        book.title = 'The Mythical man week';

        const plainBook = typedToPlain(book);
        expect(plainBook.name).toEqual(book.title);
    });

    it('skips undefined properties', () => {
        class Book {
            title: string;
            subTitle?: string;
        }
        createSchema(Book).exposeAll();

        const book = new Book();
        book.title = 'The Design of Everyday things';
        book.subTitle = undefined;
        const plainBook = typedToPlain(book, { skipUndefinedProperties: true });
        expect(plainBook.title).toEqual(book.title);
        expect(plainBook.hasOwnProperty('subTitle')).toBeFalsy();
    });
});

describe('registries', () => {
    afterEach(() => {
        defaultSchemaRegistry.clear();
    });

    it('have separate schemas that can be selected for transforming', () => {
        class User {
            id: number;
            firstName: string;
            lastName: string;
            password: string;
        }

        const user = new User();
        user.firstName = 'Tim';
        user.lastName = 'Mundt';
        user.password = 'imsuperman';

        const registry1 = createRegistry();
        const schema1 = createSchema(User, registry1);
        schema1.getField('password').exclude();

        const registry2 = createRegistry();

        const plainUser1 = typedToPlain(user, { registry: registry1 });
        expect(plainUser1.password).toBeUndefined();

        const plainUser2 = typedToPlain(user, { registry: registry2 });
        expect(plainUser2.password).toBe('imsuperman');
    });

    it('have a default registry if none is given', () => {
        class User {
            id: number;
            firstName: string;
            lastName: string;
            password: string;
        }

        const user = new User();
        user.firstName = 'Tim';
        user.lastName = 'Mundt';
        user.password = 'imsuperman';

        const registry1 = createRegistry();
        const schema1 = createSchema(User, registry1);
        schema1.getField('password').exclude();

        const schema2 = createSchema(User);
        schema2.getField('firstName').exclude();

        const plainUser1 = typedToPlain(user, { registry: registry1 });
        expect(plainUser1.password).toBeUndefined();

        const plainUser2 = typedToPlain(user);
        expect(plainUser2.firstName).toBeUndefined();
    });
});

describe('arrays', () => {
    afterEach(() => {
        defaultSchemaRegistry.clear();
    });

    it('as top level element are rendered correctly', () => {
        class Book {
            additionalProperty: string = 'should not be exposed';
            constructor(readonly title: string) {}
        }

        const bookSchema = createSchema(Book);
        bookSchema.getField('additionalProperty').exclude();

        const book1 = new Book('On Arrays');
        const book2 = new Book('JS vs TS');
        const book3 = new Book('Python rocks');
        const list = [book1, book2, book3];

        const plainList = typedToPlain(list);
        expect(plainList).toEqual([
            { title: 'On Arrays' },
            { title: 'JS vs TS' },
            { title: 'Python rocks' }
        ]);
    });

    it('are correctly represented as arrays in a pojo', () => {
        class Book {
            title: string;
        }

        class Author {
            firstName: string;
            lastName: string;
            books: Book[];
        }

        const book = new Book();
        book.title = 'On Arrays';
        const author = new Author();
        author.firstName = 'Tim';
        author.lastName = 'Mundt';
        author.books = [book];

        const plainAuthor = typedToPlain(author);
        expect(plainAuthor.books).toEqual([{ title: 'On Arrays' }]);
    });

    it('with pojos are transformed to arrays with typed objects', () => {
        class Book {
            title: string;
        }

        const books = [
            { title: 'On Arrays' },
            { title: 'JS vs TS' },
            { title: 'Python rocks' }
        ];

        const typedList = plainToTyped(books, Book);
        typedList.forEach(book => {
            expect(book).toEqual(jasmine.any(Book));
        });
    });

    it('with pojos in a property are transformed to arrays with typed objects', () => {
        class Author {
            name: string;
            books: Book[];
        }

        class Book {
            title: string;
        }

        const author = {
            name: 'Tim Mundt',
            books: [
                { title: 'On Arrays' },
                { title: 'JS vs TS' },
                { title: 'Python rocks' }
            ]
        };

        const schema = createSchema(Author);
        schema.getField('books').nested(Book);

        const typedAuthor = plainToTyped(author, Author);
        typedAuthor.books.forEach(book => {
            expect(book).toEqual(jasmine.any(Book));
        });
    });
});

describe('existing objects', () => {
    afterEach(() => {
        defaultSchemaRegistry.clear();
    });

    it('are used to put the source properties on (typed -> plain)', () => {
        class Thing {
            name: string;
        }

        const thing = new Thing();
        thing.name = 'A thing';

        const plainThing = { name: 'Another thing' };
        const plainThing2 = typedToPlain(thing, undefined, plainThing);
        expect(plainThing).toBe(plainThing2);
        expect(plainThing.name).toBe('A thing');
    });

    it('are used to put the source properties on (plain -> typed)', () => {
        class Thing {
            name: string;
        }

        const plainThing = { name: 'A thing' };

        const thing = new Thing();
        thing.name = 'Another thing';
        const thing2 = plainToTyped(plainThing, Thing, undefined, thing);
        expect(thing).toBe(thing2);
        expect(thing.name).toBe('A thing');
    });
});
