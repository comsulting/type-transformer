export enum Direction { PLAIN_TO_TYPED = 'PLAIN_TO_TYPED', TYPED_TO_PLAIN = 'TYPED_TO_PLAIN'}
export enum Visibility { EXPOSE = 'EXPOSE', EXCLUDE = 'EXCLUDE' }

export interface SelectOptions {
    toTypedOnly?: boolean;
    toPlainOnly?: boolean;
    [key: string]: any;
}

export interface SelectFunction {
    (obj: any, direction: Direction, options?: SelectOptions): boolean;
}

type SelectorOptionsTuple = [SelectFunction, SelectOptions | undefined];

export class Selectable {
    selectors: SelectorOptionsTuple[] = [];
    defaultVisibility: Visibility;

    /**
     * Add a select function, possibly with options, that is asked if this
     * selectable should be selected for a transformation.
     * @param selector A function that is passed the object to be transformed,
     *                 the transformation direction and the options. So the
     *                 options can be used to prametrize the function.
     * @param options An object implementing SelectOptions.
     */
    addSelector(selector: SelectFunction, options?: SelectOptions) {
        this.selectors.push([selector, options]);
    }

    /**
     * Is this selectable to be selected for the given object and direction?
     * @returns either `Visibility.EXPOSE` or `Visibility.EXCLUDE` if there
     *          is an explicit answer, `undefined` otherwise.
     */
    selected(obj: any, direction: Direction): Visibility | undefined {
        let decision = this.defaultVisibility;
        for (const [selectFn, options] of this.selectors) {
            if (!options || this.doesSelectorApply(obj, direction, options)) {
                if (!selectFn(obj, direction, options)) {
                    return Visibility.EXCLUDE;
                }
                decision = Visibility.EXPOSE;
            }
        }
        return decision;
    }

    private doesSelectorApply(obj, direction: Direction, options: SelectOptions): boolean {
        if (options.toPlainOnly && direction !== Direction.TYPED_TO_PLAIN) {
            return false;
        }
        if (options.toTypedOnly && direction !== Direction.PLAIN_TO_TYPED) {
            return false;
        }
        return true;
    }
}
