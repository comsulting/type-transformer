import { defaultSchemaRegistry, SchemaRegistry } from './registry';
import { Schema } from './schema';
import { TransformationOptions, Transformer } from './transformer';
export * from './decorators';
export * from './selection';
export { CircularRefStrategy } from './references';

export function createRegistry(): SchemaRegistry {
    return new SchemaRegistry();
}

export function createSchema(type: Function, registry?: SchemaRegistry): Schema {
    registry = registry || defaultSchemaRegistry;
    return registry.getOrCreateSchema(type);
}

export function typedToPlain<T extends Object>(
    object: T,
    options?: TransformationOptions,
    existingObject?
): any;
export function typedToPlain<T extends Object>(
    object: T[],
    options?: TransformationOptions,
    existingObject?
): any[] {
    const transformer = new Transformer(getOptions(options));
    return transformer.toPlain(object, undefined, existingObject);
}

type ClassType<T> = {
    new (...args: any[]): T;
};

export function plainToTyped<O extends Array<any>, T>(
    object: O,
    targetType: ClassType<T>,
    options?: TransformationOptions,
    existingObject?
): T[];
export function plainToTyped<O, T>(
    object: O,
    targetType: ClassType<T>,
    options?: TransformationOptions,
    existingObject?
): T;
export function plainToTyped<O, T>(
    object: O[],
    targetType: ClassType<T>,
    options?: TransformationOptions,
    existingObject?
): T[]  {
    const transformer = new Transformer(getOptions(options));
    return transformer.toTyped(object, targetType, existingObject);
}

function getOptions(options?: TransformationOptions): TransformationOptions {
    options = options || { };
    if (!options.registry) {
        options.registry = defaultSchemaRegistry;
    }
    return options;
}
