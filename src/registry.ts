import { Schema } from './schema';

export class SchemaRegistry {
    schemasByType: Map<Function, Schema> = new Map<Function, Schema>();

    getOrCreateSchema(type: Function): Schema {
        let schema = this.schemasByType.get(type);
        if (!schema) {
            schema = new Schema(this, type);
            this.schemasByType.set(type, schema);
        }
        return schema;
    }

    getSchema(type: Function): Schema | undefined {
        return this.schemasByType.get(type);
    }

    clear() {
        this.schemasByType.clear();
    }
}

export const defaultSchemaRegistry = new SchemaRegistry();
