import { defaultSchemaRegistry } from '../../src/registry';
import { typedToPlain, createSchema } from '../../src';
import { plainToTyped } from '../../src/index';

describe('method fields', () => {
    afterEach(() => {
        defaultSchemaRegistry.clear();
    });

    it('getters get transformed to plain value', () => {
        class User {
            firstName: string;
            lastName: string;
            get name() {
                return `${this.firstName} ${this.lastName}`;
            }
        }

        const user = new User();
        user.firstName = 'Tim';
        user.lastName = 'Mundt';

        createSchema(User).getField('name').expose();

        const plainUser = typedToPlain(user);
        expect(plainUser).toEqual({
            firstName: 'Tim',
            lastName: 'Mundt',
            name: 'Tim Mundt'
        });
    });

    it('setters get used to transform to typed value', () => {
        class User {
            firstName: string;
            lastName: string;
            set name(value) {
                [this.firstName, this.lastName] = value.split(' ');
            }
        }

        const plainUser = { name: 'Tim Mundt' };
        const user = plainToTyped(plainUser, User);
        expect(user.firstName).toEqual('Tim');
        expect(user.lastName).toEqual('Mundt');
    });

    it('use the return value of a method', () => {
        class User {
            firstName: string;
            lastName: string;
            getName(value) {
                return `${this.firstName} ${this.lastName}`;
            }
        }

        createSchema(User).getField('getName').expose({propName: 'name'});

        const user = new User();
        user.firstName = 'Tim';
        user.lastName = 'Mundt';

        const plainUser = typedToPlain(user);
        expect(plainUser).toEqual({
            firstName: 'Tim',
            lastName: 'Mundt',
            name: 'Tim Mundt'
        });
    });
});
