import { CircularRefStrategy, ReferenceGraph } from './references';
import { SchemaRegistry } from './registry';
import { Field, Schema } from './schema';
import { Direction, Visibility } from './selection';


export interface TransformationOptions {
    registry?: SchemaRegistry;
    circularRefStrategy?: CircularRefStrategy;
    skipUndefinedProperties?: boolean;
}

export class Transformer {
    private referenceGraph: ReferenceGraph;

    constructor (readonly options: TransformationOptions) {
        this.referenceGraph = new ReferenceGraph(options.circularRefStrategy);
    }

    toPlain(typed, sourceType?, existingTarget?) {
        let value = typed;
        if (typeof value === 'object' && value !== null) {
            if (value.toJS) {
                value = value.toJS();
            }
            const [isArray, plain] = this.getTargetObject(value, undefined, existingTarget);
            if (!sourceType && value.constructor !== Object) {
                sourceType = value.constructor;
            }
            value = this.referenceGraph.preventCircularDependencies(value, plain, () => {
                if (isArray) {
                    for (let subValue of (value as any[])) {
                        subValue = this.toPlain(subValue);
                        (plain as any[]).push(subValue);
                    }
                }
                else {
                    const fields = this.getFields(Direction.TYPED_TO_PLAIN, value, sourceType);
                    for (const [fieldName, field] of fields) {
                        this.applyFieldToPlain(value, plain, fieldName, field);
                    }
                }
                return plain;
            });
        }
        const schema = this.getSchema(sourceType);
        value = this.postProcessObject(typed, value, Direction.TYPED_TO_PLAIN, schema);
        return value;
    }

    private applyFieldToPlain(source, target, fieldName: string, field: Field | undefined) {
        let propName = fieldName;
        let subValue = source[fieldName];
        if (subValue === undefined && this.options.skipUndefinedProperties) {
            return;
        }
        if (typeof subValue === 'function') {
            subValue = source[fieldName]();
        }
        if (field) {
            propName = field.propName instanceof Function
                ? field.propName(source, Direction.TYPED_TO_PLAIN)
                : field.propName;
            const valueTransformers = field.getValueTransformers(source, Direction.TYPED_TO_PLAIN);
            for (const valueTransformer of valueTransformers) {
                subValue = valueTransformer.transform(subValue, source, Direction.TYPED_TO_PLAIN);
            }
        }
        subValue = this.toPlain(subValue);
        target[propName] = subValue;
        return target;
    }

    toTyped(plain, targetType, existingTarget?) {
        let value = plain;
        if (typeof value === 'object' && value !== null) {
            let [isArray, target] = this.getTargetObject(value, targetType, existingTarget);
            return this.referenceGraph.preventCircularDependencies(value, target, () => {
                if (isArray) {
                    for (let subValue of (value as any[])) {
                        subValue = this.toTyped(subValue, targetType);
                        (target as any[]).push(subValue);
                    }
                }
                else {
                    const fields = this.getFields(Direction.PLAIN_TO_TYPED, value, targetType);
                    for (const [fieldName, field] of fields) {
                        target = this.applyFieldToTyped(value, target, fieldName, field);
                    }
                }
                return target;
            });
        }
        const schema = this.getSchema(targetType);
        value = this.postProcessObject(plain, value, Direction.PLAIN_TO_TYPED, schema);
        return value;
    }

    private applyFieldToTyped(source, target, fieldName: string, field: Field | undefined) {
        let propName = fieldName;
        let subType;
        if (field) {
            propName = field.propName instanceof Function
            ? field.propName(source, Direction.TYPED_TO_PLAIN)
            : field.propName;
            subType = field.nestedType;
        }

        let subValue = source[propName];
        if (field) {
            const valueTransformers = field.getValueTransformers(source, Direction.PLAIN_TO_TYPED);
            for (const valueTransformer of valueTransformers) {
                subValue = valueTransformer.transform(subValue, source, Direction.PLAIN_TO_TYPED);
            }
        }
        subValue = this.toTyped(subValue, subType);
        if (target.set && target.constructor.name === 'Map') {
            target = target.set(fieldName, subValue);
        }
        else {
            target[fieldName] = subValue;
        }
        return target;
    }

    private postProcessObject(source, target, direction: Direction, schema?: Schema) {
        if (schema) {
            const postProcessors = schema.getPostProcessors(source, direction);
            for (const postProcessor of postProcessors) {
                target = postProcessor.transform(source, target, direction);
            }
        }
        return target;
    }

    private getFields(direction: Direction, object, type?: Function): Map<string, Field|undefined> {
        let defaultVisibility = Visibility.EXPOSE;
        const schema = this.getSchema(type);
        if (schema && schema.defaultVisibility) {
            defaultVisibility = schema.defaultVisibility;
        }
        const fields: Map<string, Field|undefined> = new Map<string, Field|undefined>();
        if (defaultVisibility === Visibility.EXPOSE) {
            Object.keys(object).forEach(key => fields.set(key, undefined));
        }
        if (schema) {
            for (const field of schema.fields) {
                const fieldVisibility = field.selected(object, direction) || defaultVisibility;
                if (fieldVisibility === Visibility.EXPOSE) {
                    fields.set(field.name, field);
                }
                else if (fieldVisibility === Visibility.EXCLUDE) {
                    fields.delete(field.name);
                }
            }
        }
        return fields;
    }

    private getSchema(type?: Function) {
        return type ? this.options.registry!.getSchema(type) : undefined;
    }

    /**
     * Get the (empty) object to put the source values on.
     * @param value The source value
     * @param targetType The optional type of the target
     * @param existingTarget An object that should be used as target
     */
    private getTargetObject(value, targetType?, existingTarget?): [boolean, object] {
        const isArray = value instanceof Array || value instanceof Set;
        const target = existingTarget ? existingTarget :
                       isArray ? [] :
                       targetType ? new targetType() :
                       { };
        return [isArray, target];
    }
}
