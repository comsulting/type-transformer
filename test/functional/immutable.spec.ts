import { Map } from 'immutable';
import { defaultSchemaRegistry } from '../../src/registry';
import { typedToPlain, createSchema, plainToTyped } from '../../src';

describe('immutable.js handling', () => {
    afterEach(() => {
        defaultSchemaRegistry.clear();
    });

    it('transforms an object that contains an immutable.js Map', () => {
        class Person {
            name: string;
            features: Map<string, string>;
        }

        let person = new Person();
        person.name = 'Famous Person';
        person.features = Map({nationality: 'German', homeplanet: 'Earth'});

        const plainPerson = typedToPlain(person);
        expect(plainPerson.features).toEqual({
            nationality: 'German',
            homeplanet: 'Earth'
        });
    });

    it('transforms an object with on object to an immutable.js Map', () => {
        class Person {
            name: string;
            features: Map<string, string>;
        }

        const schema = createSchema(Person);
        schema.getField('features').expose().nested(Map);

        let plainPerson = {
            name: 'Famous Person',
            features: {nationality: 'German', homeplanet: 'Earth'}
        };

        const person = plainToTyped(plainPerson, Person);
        expect(person).toEqual(jasmine.any(Person));
        expect(person.features).toEqual(jasmine.any(Map));
        expect(person.features.get('nationality')).toEqual('German');
        expect(person.features.get('homeplanet')).toEqual('Earth');
    });
});
