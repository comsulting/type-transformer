import { defaultSchemaRegistry } from './registry';
import { ExposeOptions } from './schema';
import { SelectFunction } from './selection';

/**
 * Marks a property or class as included in the process of transformation.
 */
export function expose(options?: ExposeOptions) {
    return function (object: Object | Function, propertyName?: string): void {
        if (object instanceof Function) {
            const schema = defaultSchemaRegistry.getOrCreateSchema(object);
            schema.exposeAll();
        }
        else if (propertyName) {
            const schema = defaultSchemaRegistry.getOrCreateSchema(object.constructor);
            schema.getField(propertyName).expose(options);
        }
    };
}

/**
 * Marks a property or class as excluded in the process of transformation.
 */
export function exclude() {
    return function (object: Object | Function, propertyName?: string): void {
        if (object instanceof Function) {
            const schema = defaultSchemaRegistry.getOrCreateSchema(object);
            schema.excludeAll();
        }
        else if (propertyName) {
            const schema = defaultSchemaRegistry.getOrCreateSchema(object.constructor);
            schema.getField(propertyName).exclude();
        }
    };
}

/**
 * Define a function that dynamically selects a property.
 */
export function select(selectFn: SelectFunction) {
    return function (object: Object | Function, propertyName?: string): void {
        if (propertyName) {
            const schema = defaultSchemaRegistry.getOrCreateSchema(object.constructor);
            schema.getField(propertyName).select(selectFn);
        }
        else {
            throw 'Cannot use "@select" on a class.';
        }
    };
}

/**
 * Creates a schema for a class that inherits from the schemas of the parent classes.
 */
export function inherit() {
    return function (object: Function): void {
        const schema = defaultSchemaRegistry.getOrCreateSchema(object);
        schema.inherit();
    };
}
