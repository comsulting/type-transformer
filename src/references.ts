export enum CircularRefStrategy {
    NONE = 'NONE',  // do not check for circular references at all
    BREAK = 'BREAK',  // omit objects that would introduce a circular reference (default)
    RESOLVE = 'RESOLVE',  // rebuild the same circular reference in the target
    ERROR = 'ERROR'  // throw an Error when a circular reference is detected
}

class ReferenceNode {
    referencedBy: Map<Object, ReferenceNode> = new Map<Object, ReferenceNode>();
    targetObject: any;

    constructor (readonly object: Object, referencedBy?: ReferenceNode) {
        if (referencedBy) {
            this.referencedBy.set(referencedBy.object, referencedBy);
        }
    }

    /**
     * Checks if the object of this node is referenced by the given object.
     */
    isReferencedBy(object: Object): boolean {
        if (this.referencedBy.has(object)) {
            return true;
        }
        for (const reference of this.referencedBy.values()) {
            if (reference.isReferencedBy(object)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the object that references the given one, if any.
     */
    getReferencing(object: Object) {
        if (this.referencedBy.has(object)) {
            return this.referencedBy.get(object)!.targetObject;
        }
        for (const reference of this.referencedBy.values()) {
            const referencing = reference.getReferencing(object);
            if (referencing) {
                return referencing;
            }
        }
    }

    addReferencer(node: ReferenceNode) {
        this.referencedBy.set(node.object, node);
    }
}

export class ReferenceGraph {
    private nodeStack: ReferenceNode[] = [];
    private allNodes: Map<Object, ReferenceNode> = new Map<Object, ReferenceNode>();
    private circularStrategy: CircularRefStrategy = CircularRefStrategy.BREAK;

    constructor (circularStrategy?: CircularRefStrategy) {
        if (circularStrategy) {
            this.circularStrategy = circularStrategy;
        }
    }

    get current() {
        if (this.nodeStack.length) {
            return this.nodeStack[this.nodeStack.length - 1];
        }
    }

    /**
     * Check if a value would introduce a circular dependency.
     * If so, react according to the configured strategy (@see CircularRefStrategy).
     * If not, call the @param handleValue() function.
     * @param targetObject the (empty) object that is populated by handleValue
     * @returns the result of either the handling of a circular dependency
     *          or the @param targetObject
     */
    preventCircularDependencies(value, targetObject, handleValue: Function) {
        if (this.circularStrategy !== CircularRefStrategy.NONE
            && this.isCircularReference(value)) {
                return this.handleCircularReference(value);
        }
        else {
            const refNode = this.descend(value);
            refNode.targetObject = targetObject;
            const result = handleValue();
            this.ascend();
            return result;
        }
    }

    descend(object: Object): ReferenceNode {
        const currentNode = this.current;
        let node = this.allNodes.get(object);
        if (node && currentNode) {
            node.addReferencer(currentNode);
        }
        else {
            node = new ReferenceNode(object, currentNode);
            this.allNodes.set(object, node);
        }
        this.nodeStack.push(node);
        return node;
    }

    ascend() {
        this.nodeStack.pop();
    }

    isCircularReference(referred: Object) {
        if (!this.allNodes.has(referred)) {
            return false;
        }
        return this.current!.isReferencedBy(referred);
    }

    handleCircularReference(referred: Object) {
        switch (this.circularStrategy) {
            case CircularRefStrategy.BREAK:
                return undefined;
            case CircularRefStrategy.ERROR:
                throw new Error('A circular reference was detected.');
            case CircularRefStrategy.RESOLVE:
                return this.current!.getReferencing(referred);
            default:
                throw new Error('Unkown strategy for handling circular references: '
                                + this.circularStrategy);
        }
    }
}
