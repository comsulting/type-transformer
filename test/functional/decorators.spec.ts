import { defaultSchemaRegistry } from '../../src/registry';
import { exclude, typedToPlain, expose, select } from '../../src';

describe('decorators', () => {
    afterEach(() => {
        defaultSchemaRegistry.clear();
    });

    it('excludes property', () => {
        @expose()
        class User {
            id: number;
            firstName: string;
            lastName: string;
            @exclude()
            password: string;
        }

        const user = new User();
        user.id = 1;
        user.firstName = 'Tim';
        user.lastName = 'Mundt';
        user.password = 'imsuperman';

        const plainUser = typedToPlain(user);
        expect(plainUser.password).toBeUndefined();
    });

    it('includes property', () => {
        @exclude()
        class User {
            id: number;
            firstName: string;
            @expose()
            lastName: string;
            password: string;
        }

        const user = new User();
        user.id = 1;
        user.firstName = 'Tim';
        user.lastName = 'Mundt';
        user.password = 'imsuperman';

        const plainUser = typedToPlain(user);
        expect(plainUser).toEqual({
            lastName: 'Mundt'
        });
    });

    it('dynamically decide upon inclusion', () => {
        class Product {
            id: number;
            @exclude()
            isInStore: boolean;
            @select(obj => obj.isInStore)
            itemsRemaining: number;
        }

        const product1 = new Product();
        product1.id = 1;
        product1.isInStore = false;
        product1.itemsRemaining = 0;

        const product2 = new Product();
        product2.id = 2;
        product2.isInStore = true;
        product2.itemsRemaining = 3;

        const plainProduct1 = typedToPlain(product1);
        expect(plainProduct1).toEqual({
            id: 1
        });

        const plainProduct2 = typedToPlain(product2);
        expect(plainProduct2).toEqual({
            id: 2,
            itemsRemaining: 3
        });
    });
});
