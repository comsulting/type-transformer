import { defaultSchemaRegistry } from '../../src/registry';
import { typedToPlain, createSchema, Direction, plainToTyped } from '../../src';

describe('selection', () => {
    afterEach(() => {
        defaultSchemaRegistry.clear();
    });

    it('selects a property based on direction', () => {
        class Person {
            name: string;
            age: number;
        }
        createSchema(Person).excludeAll().getField('age').expose({ toPlainOnly: true });

        let meemaw = new Person();
        meemaw.name = 'meemaw';
        meemaw.age = 84;

        // typed => plain: meemaw's age gets exposed
        const plainMeemaw = typedToPlain(meemaw);
        expect(plainMeemaw.age).toBe(84);

        // plain => typed: meemaw's age doesn't get exposed
        meemaw = plainToTyped(plainMeemaw as Object, Person);
        expect(meemaw.age).toBeUndefined();
    });

    it('deselects a property based on direction', () => {
        class Person {
            name: string;
            age: number;
        }
        createSchema(Person).getField('age').exclude({ toTypedOnly: true });

        let meemaw = new Person();
        meemaw.name = 'meemaw';
        meemaw.age = 84;

        // typed => plain: meemaw's age doesn't get excluded (i.e. it's exposed)
        const plainMeemaw = typedToPlain(meemaw);
        expect(plainMeemaw.age).toBe(84);

        // plain => typed: meemaw's age gets excluded
        meemaw = plainToTyped(plainMeemaw as Object, Person);
        expect(meemaw.age).toBeUndefined();
    });

    it('dynamically selects a property', () => {
        class Person {
            name: string;
            age: number;
        }
        createSchema(Person).getField('age').select((obj) => {
            return obj.age > 10;
        });

        const meemaw = new Person();
        meemaw.name = 'meemaw';
        meemaw.age = 84;

        const babe = new Person();
        babe.name = 'babe';
        babe.age = 1;

        const plainMeemaw = typedToPlain(meemaw);
        expect(plainMeemaw.age).toBe(84);
        const plainBabe = typedToPlain(babe);
        expect(plainBabe.age).toBeUndefined();
    });

    it('dynamically selects a property based on direction', () => {
        class Person {
            name: string;
            age: number;
        }
        createSchema(Person).getField('age').select((obj, direction) => {
            if (direction === Direction.TYPED_TO_PLAIN) {
                return obj.age > 10;
            }
            return obj.age < 10;
        });

        let meemaw = new Person();
        meemaw.name = 'meemaw';
        meemaw.age = 84;

        let babe = new Person();
        babe.name = 'babe';
        babe.age = 1;

        // typed => plain: meemaw's age gets exposed, babe's doesn't
        const plainMeemaw = typedToPlain(meemaw);
        expect(plainMeemaw.age).toBe(84);
        const plainBabe = typedToPlain(babe);
        expect(plainBabe.age).toBeUndefined();

        // plain => typed: babes's age gets transformed, meemaw's doesn't
        plainBabe.age = 1;
        meemaw = plainToTyped(plainMeemaw as Object, Person);
        expect(meemaw.age).toBeUndefined();
        babe = plainToTyped(plainBabe as Object, Person);
        expect(babe.age).toBe(1);
    });

    it('transforms a value with a transform function only in one direction', () => {
        class Person {
            name: string;
        }
        createSchema(Person).getField('name').transform(
            value => value.toLowerCase(),
            { toPlainOnly: true }
        );

        let person = new Person();
        person.name = 'Famous Person';

        // typed => plain: person's name gets lowercased
        const plainPerson = typedToPlain(person);
        expect(plainPerson.name).toBe('famous person');

        // plain => typed: person's name doesn't get transformed
        plainPerson.name = 'Mixed Case Name';
        person = plainToTyped(plainPerson as Object, Person);
        expect(person.name).toBe('Mixed Case Name');
    });
});
