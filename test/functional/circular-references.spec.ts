import { typedToPlain } from '../../src';
import { defaultSchemaRegistry } from '../../src/registry';
import { CircularRefStrategy } from '../../src/references';


describe('circular reference handling', () => {
    afterEach(() => {
        defaultSchemaRegistry.clear();
    });

    it('omits a field with an object that references the current object', () => {
        class Author {
            id: number;
            firstName: string;
            lastName: string;
            book: Book;
        }

        class Book {
            title: string;
            author: Author;
        }

        const author = new Author();
        author.firstName = 'Tim';
        author.lastName = 'Mundt';

        const book = new Book();
        book.title = 'How to break circular dependencies';
        book.author = author;

        // circular reference
        author.book = book;

        const plainAuthor = typedToPlain(author);
        expect(plainAuthor).toEqual({
            firstName: 'Tim',
            lastName: 'Mundt',
            book: {
                title: 'How to break circular dependencies',
                author: undefined
            }
        });
    });

    it('omits a field with an object that references the current object indirectly', () => {
        class Author {
            id: number;
            firstName: string;
            lastName: string;
            book: Book;
        }

        class Publisher {
            name: string;
            owner: Author;
        }

        class Book {
            title: string;
            publisher: Publisher;
        }

        const author = new Author();
        author.firstName = 'Tim';
        author.lastName = 'Mundt';

        const publisher = new Publisher();
        publisher.name = 'Snotty Publishing';
        publisher.owner = author;

        const book = new Book();
        book.title = 'How to break circular dependencies';
        book.publisher = publisher;

        // circular reference
        author.book = book;

        const plainAuthor = typedToPlain(author);
        expect(plainAuthor).toEqual({
            firstName: 'Tim',
            lastName: 'Mundt',
            book: {
                title: 'How to break circular dependencies',
                publisher: {
                    name: 'Snotty Publishing',
                    owner: undefined
                }
            }
        });
    });

    it('resolves circular references by reusing objects', () => {
        class Author {
            id: number;
            firstName: string;
            lastName: string;
            book: Book;
        }

        class Book {
            title: string;
            author: Author;
        }

        const author = new Author();
        author.firstName = 'Tim';
        author.lastName = 'Mundt';

        const book = new Book();
        book.title = 'How to break circular dependencies';
        book.author = author;

        // circular reference
        author.book = book;

        const plainAuthor = typedToPlain(author,
            { circularRefStrategy: CircularRefStrategy.RESOLVE });
        expect(plainAuthor.book.author).toBe(plainAuthor);
    });

    it('resolves indirect circular references by reusing objects', () => {
        class Author {
            id: number;
            firstName: string;
            lastName: string;
            book: Book;
        }

        class Publisher {
            name: string;
            owner: Author;
        }

        class Book {
            title: string;
            publisher: Publisher;
        }

        const author = new Author();
        author.firstName = 'Tim';
        author.lastName = 'Mundt';

        const publisher = new Publisher();
        publisher.name = 'Snotty Publishing';
        publisher.owner = author;

        const book = new Book();
        book.title = 'How to break circular dependencies';
        book.publisher = publisher;

        // circular reference
        author.book = book;

        const plainAuthor = typedToPlain(author,
            { circularRefStrategy: CircularRefStrategy.RESOLVE });
        expect(plainAuthor.book.publisher.owner).toBe(plainAuthor);
    });

    it('throws an error when detecting a circular reference', () => {
        class Author {
            id: number;
            firstName: string;
            lastName: string;
            book: Book;
        }

        class Book {
            title: string;
            author: Author;
        }

        const author = new Author();
        author.firstName = 'Tim';
        author.lastName = 'Mundt';

        const book = new Book();
        book.title = 'How to break circular dependencies';
        book.author = author;

        // circular reference
        author.book = book;

        expect(() => {
            typedToPlain(author, { circularRefStrategy: CircularRefStrategy.ERROR });
        }).toThrow(new Error('A circular reference was detected.'));
    });

    it('doesn\'t bother checking for circular references', () => {
        class Author {
            id: number;
            firstName: string;
            lastName: string;
            book: Book;
        }

        class Book {
            title: string;
            author: Author;
        }

        const author = new Author();
        author.firstName = 'Tim';
        author.lastName = 'Mundt';

        const book = new Book();
        book.title = 'How to break circular dependencies';
        book.author = author;

        // circular reference
        author.book = book;

        expect(() => {
            typedToPlain(author, { circularRefStrategy: CircularRefStrategy.NONE });
        }).toThrow();  // throws an error due to too much recursion
    });

    it('fails with invalid strategy', () => {
        class Author {
            id: number;
            firstName: string;
            lastName: string;
            book: Book;
        }

        class Book {
            title: string;
            author: Author;
        }

        const author = new Author();
        author.firstName = 'Tim';
        author.lastName = 'Mundt';

        const book = new Book();
        book.title = 'How to break circular dependencies';
        book.author = author;

        // circular reference
        author.book = book;

        expect(() => {
            typedToPlain(author, { circularRefStrategy: 'invalid' as CircularRefStrategy });
        }).toThrow();  // throws an error due to too much recursion
    });

    it('exposes same value twice if references are not circular', () => {
        class Person {
            id: number;
            firstName: string;
            lastName: string;
            publisher: Person;
        }

        class Book {
            title: string;
            author: Person;
        }

        const person = new Person();
        person.firstName = 'Tim';
        person.lastName = 'Mundt';

        const book1 = new Book();
        book1.title = 'How to break circular dependencies';
        book1.author = person;

        const book2 = new Book();
        book2.title = 'How to break circular dependencies (2nd Edition)';
        book2.author = person;

        typedToPlain([book1, book2]);  // no exception here
    });
});
