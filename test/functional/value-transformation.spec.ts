import { defaultSchemaRegistry } from '../../src/registry';
import { typedToPlain, createSchema, Direction, plainToTyped } from '../../src';

describe('value transformation', () => {
    afterEach(() => {
        defaultSchemaRegistry.clear();
    });

    it('transforms a value with a transform function', () => {
        class Person {
            name: string;
        }
        createSchema(Person).getField('name').transform((value, obj, direction) => {
            if (direction === Direction.TYPED_TO_PLAIN) {
                return obj.name.toLowerCase();
            }
            return obj.name.toUpperCase();
        });

        let person = new Person();
        person.name = 'Famous Person';

        // typed => plain: person's name gets lowercased
        const plainPerson = typedToPlain(person);
        expect(plainPerson.name).toBe('famous person');

        // plain => typed: person's name gets upperacased
        person = plainToTyped(plainPerson as Object, Person);
        expect(person.name).toBe('FAMOUS PERSON');
    });

    it('post processes an object with a transform function', () => {
        class Person {
            hasPhd: boolean;
            name: string;
        }
        createSchema(Person).postProcess((source, target, direction) => {
            if (direction === Direction.TYPED_TO_PLAIN) {
                if (source.hasPhd) {
                    target.name = target.name + ', Ph.D.';
                }
            }
            return target;
        });

        let person = new Person();
        person.name = 'Famous Person';
        person.hasPhd = true;

        const plainPerson = typedToPlain(person);
        expect(plainPerson.name).toBe('Famous Person, Ph.D.');
    });
});
