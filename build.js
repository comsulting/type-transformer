'use strict';

const fs = require('fs');
const del = require('del');
const rollup = require('rollup');
const typescript = require('rollup-plugin-typescript2');
const minify = require('uglify-es');
const uglify = require('rollup-plugin-uglify');
const pkg = require('./package.json');

let plugins = [
    typescript({
        tsconfigOverride: {
            compilerOptions: {
                module: 'esnext'
            }
        }
    })
];

let envs = [
    {
        fileExt: '.js',
        plugins: plugins,
        sourcemap: true
    },
    {
        fileExt: '.min.js',
        plugins: [...plugins, uglify.uglify({}, minify.minify)],
        sourcemap: false
    }
];

let targets = [
    {
        file: 'dist/type-transformer',
        format: 'umd',
    },
    {
        file: 'dist/type-transformer.commonjs',
        format: 'cjs',
    },
    {
        file: 'dist/type-transformer.es',
        format: 'es',
    }
];

let configs = [];
for (let env of envs) {
    let outputs = [];
    for (let target of targets) {
        outputs.push({
            file: target.file + env.fileExt,
            name: 'type-transformer',
            format: target.format,
            sourcemap: env.sourcemap
        });
    }
    configs.push({
        input: './src/index.ts',
        output: outputs,
        plugins: env.plugins
    });
}

Promise.all(configs.map(
    config => rollup.rollup(config).then(
        bundle => Promise.all(config.output.map(
            outputOptions => bundle.write(outputOptions)
        ))
    )
))
.then(() => {
    // copy package.json, LICENSE, and README.md
    delete pkg.private;
    delete pkg.devDependencies;
    delete pkg.scripts;
    fs.writeFileSync('dist/package.json', JSON.stringify(pkg, null, '  '), 'utf-8');
    fs.writeFileSync('dist/LICENSE', fs.readFileSync('LICENSE', 'utf-8'), 'utf-8');
    fs.writeFileSync('dist/README.md', fs.readFileSync('README.md', 'utf-8'), 'utf-8');

    // move type declarations to dist and cleanup
    for (const file of fs.readdirSync('dist/src')) {
        fs.renameSync('dist/src/' + file, 'dist/' + file);
    }
    del(['dist/src', 'dist/test']);
})
.catch(error => console.error(error));
